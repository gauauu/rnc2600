#!/usr/bin/python

import xml.etree.ElementTree as etree    
import sys
import os
import re
import ntpath

#range is 32 to 120

def level_to_data(input_file):
    file_name = ntpath.basename(input_file)
    level_id = os.path.splitext(file_name)[0]

    input_file = open(input_file, "r")
    output_file = open("bin/gen/" + level_id + ".s", "w")

    to_zero = re.compile("/[.]/")
    to_one = re.compile("/[^.]/")

    #calculate positions
    MIN = 32
    MAX = 130
    STEP = ((MAX - MIN) * 1.0) / 8.0

    
    ALL = 0b10000000
    NUSIZ_1       = ALL + 0
    NUSIZ_2_CLOSE = ALL + 1
    NUSIZ_2_MED   = ALL + 2
    NUSIZ_3_CLOSE = ALL + 3
    NUSIZ_2_WIDE  = ALL + 4
    NUSIZ_DOUBLE  = ALL + 5
    NUSIZ_3_MED   = ALL + 6
    NUSIZ_QUAD    = ALL + 7
    
    POS_ITEM      = 128
    POS_LASER     = 129
    POS_LAST      = MIN + int(round((STEP * 7)))

    result_pos = []
    result_nusiz = []

    result_pos_b = []
    result_nusiz_b = []

    uses_alternating = False

    line_ctr = -1;
    for line in input_file:
        line = line.rstrip()

        if line == "XXXXXXXX":
            print("found item!\n")
            result_nusiz.insert(0, POS_ITEM)
            result_pos.insert(0, POS_ITEM)

            result_nusiz_b.insert(0, POS_ITEM)
            result_pos_b.insert(0, POS_ITEM)
        else:

            orig_line = line
            line = re.sub("[.]", "0", line)
            line = re.sub("[^0]", "1", line)
            spikes = list(map(int, list(line)))
            first_pos = next((i for i, x in enumerate(spikes) if x), None)
            if first_pos == None:
                result_pos.insert(0,0)
                result_nusiz.insert(0,0)

                result_pos_b.insert(0,0)
                result_nusiz_b.insert(0,0)
            else:
                #special cases
                if orig_line == ">...*..<":
                    uses_alternating = True
                    result_pos.insert(0,MIN)
                    result_nusiz.insert(0,NUSIZ_2_WIDE)
                    result_pos_b.insert(0,POS_LAST)
                    result_nusiz_b.insert(0,NUSIZ_1)
                elif line == "01111110":
                    result_pos.insert(0,POS_LASER)
                    result_nusiz.insert(0,POS_LASER)
                    result_pos_b.insert(0,POS_LASER)
                    result_nusiz_b.insert(0,POS_LASER)
                elif orig_line == ">......<":
                    uses_alternating = True
                    result_pos.insert(0,MIN)
                    result_nusiz.insert(0,NUSIZ_1)
                    result_pos_b.insert(0,POS_LAST)
                    result_nusiz_b.insert(0,NUSIZ_1)
                elif orig_line == ">****...":
                    result_pos.insert(0,MIN)
                    result_nusiz.insert(0,NUSIZ_3_MED)
                    result_pos_b.insert(0,MIN)
                    result_nusiz_b.insert(0,NUSIZ_3_MED)
                elif orig_line == "...****<":
                    result_pos.insert(0,MIN + int(round((STEP * first_pos))))
                    result_nusiz.insert(0,NUSIZ_3_MED)
                    result_pos_b.insert(0,MIN + int(round((STEP * first_pos))))
                    result_nusiz_b.insert(0,NUSIZ_3_MED)
                elif orig_line == ">*......":
                    result_pos.insert(0,MIN)
                    result_nusiz.insert(0,NUSIZ_2_CLOSE)
                    result_pos_b.insert(0,MIN)
                    result_nusiz_b.insert(0,NUSIZ_2_CLOSE)
                elif orig_line == "..*...*<":
                    uses_alternating = True
                    result_pos.insert(0,MIN + int(round((STEP * first_pos))))
                    result_nusiz.insert(0,NUSIZ_2_WIDE)
                    result_pos_b.insert(0,POS_LAST)
                    result_nusiz_b.insert(0,NUSIZ_1)
                elif orig_line == ">.*....<":
                    uses_alternating = True
                    result_pos.insert(0,MIN)
                    result_nusiz.insert(0,NUSIZ_2_MED)
                    result_pos_b.insert(0,POS_LAST)
                    result_nusiz_b.insert(0,NUSIZ_1)
                else:
                    result_pos.insert(0,MIN + int(round((STEP * first_pos))))
                    result_nusiz.insert(0,0b10000000)

                    result_pos_b.insert(0,MIN + int(round((STEP * first_pos))))
                    result_nusiz_b.insert(0,0b10000000)
        
        line_ctr = line_ctr + 1

    for i in range(0,15):
        result_pos.insert(0,0)
        result_nusiz.insert(0,0)

        result_pos_b.insert(0,0)
        result_nusiz_b.insert(0,0)

    output_file.write(".segment \"LEVELS\" \n")
    output_file.write(".align 256\n")

    output_file.write(".export " + level_id + "Pos\n")
    output_file.write(".export _" + level_id + "Pos\n")
    output_file.write(".export " + level_id + "PosB\n")
    output_file.write(".export _" + level_id + "PosB\n")

    output_file.write(level_id + "Pos:\n")
    output_file.write("_" + level_id + "Pos:\n")

    #if we're not using alternating, use the label here, 
    #so we don't waste rom
    if uses_alternating == False:
        output_file.write(level_id + "PosB:\n")
        output_file.write("_" + level_id + "PosB:\n")

    for row_val in result_pos:
        output_file.write(" .byt " + str(row_val) + "\n")

    output_file.write("\n\n\n")

    output_file.write(".export " + level_id + "Nusiz\n")
    output_file.write(".export _" + level_id + "Nusiz\n")
    output_file.write(".export " + level_id + "NusizB\n")
    output_file.write(".export _" + level_id + "NusizB\n")

    output_file.write(level_id + "Nusiz:\n")
    output_file.write("_" + level_id + "Nusiz:\n")

    #if we're not using alternating, use the label here, 
    #so we don't waste rom
    if uses_alternating == False:
        output_file.write(level_id + "NusizB:\n")
        output_file.write("_" + level_id + "NusizB:\n")

    for row_val in result_nusiz:
        output_file.write(" .byt " + str(row_val) + "\n")


 
    output_file.write("\n\n\n")
    #if we ARE using alternating, add frame B here. 
    if uses_alternating == True:
        output_file.write(".align 256\n")
        output_file.write(level_id + "PosB:\n")
        output_file.write("_" + level_id + "PosB:\n")

        for row_val in result_pos_b:
            output_file.write(" .byt " + str(row_val) + "\n")

        output_file.write("\n\n\n")
        output_file.write(level_id + "NusizB:\n")
        output_file.write("_" + level_id + "NusizB:\n")

        for row_val in result_nusiz_b:
            output_file.write(" .byt " + str(row_val) + "\n")

        output_file.write("\n\n\n")


    output_file.write(".segment \"CODE\" \n")
    output_file.write(".export _"  + level_id + "_max\n")
    output_file.write(".export "  + level_id + "_max\n")
    output_file.write("_"  + level_id + "_max:\n")
    output_file.write(level_id + "_max:\n")
    output_file.write("  .byt " + str(line_ctr) + "\n")


    
    input_file.close()
    output_file.close()




if (len(sys.argv) <= 1):
        print("Usage level2Data.py [level_file]\n")
        exit(1)

if __name__ == "__main__":
    level_file = sys.argv[1]
    level_to_data(level_file)
