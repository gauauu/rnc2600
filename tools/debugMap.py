#!/usr/bin/python3
import sys

def convert_maps(base):
    labels_file = open("labels.txt", "r")
    label_lines = labels_file.readlines()

    labels = {}
    
    for line in label_lines:
        parts = line.split()
        label = parts[2][1:]
        addr = parts[1][2:].lower()
        labels[label] = addr


    sym_file = open(base + ".sym", "w")
    for label, addr in labels.items():
        sym_file.write(label + "        " + addr + "        (R )\n")


if __name__ == "__main__":
    convert_maps(sys.argv[1])
