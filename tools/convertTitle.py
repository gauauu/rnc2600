#!/usr/bin/python3
from PIL import Image
import os

byte_ctr = 0

def output_column(symbol, im, col, pix):
    global byte_ctr

    start_bit = col * 8
    end_bit = start_bit + 8

    lines = symbol + ":\n"
    for y in range(im.height - 1, -1, -1):
        line = "    .byte %"
        byte_ctr = byte_ctr + 1
        for x in range(start_bit, end_bit):
            
            pix_val = pix[x,y]
            #print (pix_val)
            bit_val = "0"
            if pix_val[0] == 0:
                bit_val = "1"

            line = line + bit_val
        lines = lines + "\n" + line

    if byte_ctr > 255:
        lines = ".align 256\n" + lines
        byte_ctr = 0

    return(lines)


def convert_png(file_name, out_file):

    base = os.path.basename(file_name)
    symbol = os.path.splitext(base)[0]

    im = Image.open("bmp/" + file_name) # Can be many different formats.
    pix = im.load()

    width,height = im.size
    print (symbol + " has height " + str(height))


    for col in range(0,6):
        out_file.write(".export bmp_" + symbol + "_" + str(col) + "\n")
        label = "bmp_" + symbol + "_" + str(col)
        out_file.write(output_column(label, im, col, pix) + "\n")

def convert_bmp_gfx():
    out_file = open("bin/gen/bmp.s", "w")
    out_file.write('.segment "TITLEGFX"\n')
    out_file.write('.align 256\n')
    for file_name in sorted(os.listdir("bmp")):
        if file_name.endswith(".png"):
            convert_png(file_name, out_file)



if __name__ == "__main__":
    convert_bmp_gfx()

#print (im.size)  # Get the width and hight of the image for iterating over
#print (pix[10,10])  # Get the RGBA Value of the a pixel of an image
