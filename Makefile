#!/usr/bin/make -f

###################################
# Makefile for robo-ninja-climb
###################################

####################################
# Project-specific configurations
####################################

title = robo-ninja-climb


####################################
# Build tool and path configuration
####################################
ifndef CC65_PATH
$(error Environment variable CC65_PATH is not set - must be set to the root path of cc65 installation)
endif

CC65_SHARE = $(CC65_PATH)/share/cc65

OBJDIR = bin/obj
GENDIR = bin/gen
SRCDIR = src
GFXDIR = gfx
PALDIR = gfx/pal
DATADIR = data
CBUILDFLAGS = -g -Oir -I $(CC65_SHARE)/include -I $(GENDIR) -I $(SRCDIR)
SBUILDFLAGS = -g -I $(CC65_SHARE)/asminc -I $(GENDIR)
CA65 = ca65 $(SBUILDFLAGS)
CC65 = cc65 $(CBUILDFLAGS)
LD65 = ld65
LINKFLAGS = -L $(CC65_PATH)/share/cc65/lib -m map.txt -Ln labels.txt --dbgfile $(title).dbg


#########################################
# Wildcard classes
#########################################

C_ASMS   = $(patsubst $(SRCDIR)/%.c, $(GENDIR)/%.s, $(wildcard $(SRCDIR)/*.c))
C_OBJS   = $(patsubst $(SRCDIR)/%.c, $(OBJDIR)/%.o, $(wildcard $(SRCDIR)/*.c))
ASM_OBJS = $(patsubst $(SRCDIR)/%.s, $(OBJDIR)/%.o, $(wildcard $(SRCDIR)/*.s))
DATA		 = $(wildcard $(DATADIR)/*.*)
LEVELS	 = $(patsubst levels/%.lvl, $(OBJDIR)/%.o, $(wildcard levels/*.lvl)) #$(OBJDIR)/levelPointers.o
HEADERS  = $(wildcard $(SRCDIR)/*.h) $(wildcard $(SRCDIR)/*.inc)
TEXTDATA = $(OBJDIR)/textData.o
BMPINPUT = $(wildcard gfx/%.png)
BMPOUTPUT= $(OBJDIR)/bmp.o

MKDIRS   = @mkdir -p $(OBJDIR) && mkdir -p $(GENDIR)

ALL_OBJS = $(C_OBJS) $(ASM_OBJS) $(BMPOUTPUT)

#####################################
# Emulators, running, etc
#####################################

ifndef EMU
EMU = stella
endif

#####################################
# Main Targets
#####################################

.PHONY:	run clean maps debug nocash

#link the objs 


$(title).bin : $(ALL_OBJS) $(DATA) $(LEVELS)
	rm -rf src/.*.*.sw*
	$(LD65) $(LINKFLAGS) -C rnc.cfg $(ALL_OBJS) $(LEVELS) -o $@
	
debug: $(title).bin $(title).sym
	echo `ag DEBUG:`
	stella -break `grep DEBUG labels.txt | awk '{print $$2}'` $(title).bin


run: $(title).bin $(title).sym
	$(EMU) $<


clean: 
	-rm -rf $(OBJDIR) $(GENDIR) *.bin *.nl map.txt labels.txt


########################################
# Compile rules
########################################

#generate sym file
$(title).sym: $(title).bin
	python tools/debugMap.py $(title)

#compile C into transitional assembly
$(GENDIR)/%.s : $(SRCDIR)/%.c $(HEADERS) 
	$(MKDIRS)
	$(CC65) $(BUILDFLAGS) $< -o $@

#compile generated C into transitional assembly
$(GENDIR)/%.s : $(GENDIR)/%.c $(HEADERS)
	$(MKDIRS)
	$(CC65) $(BUILDFLAGS) $< -o $@

#compile C-generated assembly into obj
$(OBJDIR)/%.o : $(GENDIR)/%.s $(HEADERS)
	$(MKDIRS)
	$(CA65) $(BUILDFLAGS) $< -o $@

#compile assembly into obj
$(OBJDIR)/%.o : $(SRCDIR)/%.s $(HEADERS)
	$(MKDIRS)
	$(CA65) $(BUILDFLAGS) $< -o $@

$(OBJDIR)/data.o : $(SRCDIR)/data.s $(HEADERS) $(DATA)
	$(MKDIRS)
	$(CA65) $(BUILDFLAGS) $< -o $@

#chr.o requires chr to have been generated
$(OBJDIR)/chr.o : $(GENDIR)/sprite.chr $(GENDIR)/sprite2.chr

#build gfx with tepples' pilbmp2nes
$(GENDIR)/%.chr: $(GFXDIR)/%.png
	python tools/pilbmp2nes.py $< $@

#convert level text format to binary source
$(GENDIR)/%.s : levels/%.lvl tools/level2Data.py
	$(MKDIRS)
	python tools/level2Data.py $<


$(GENDIR)/bmp.s: $(BMPINPUT) tools/convertTitle.py
	$(MKDIRS)
	python tools/convertTitle.py

