.include "vcs.inc"
.include "constants.inc"

.segment "ZEROPAGE"

kernel_lineCtr: .res 1
kernel_blockCtr: .res 1
kernel_blockIndex: .res 1

kernel_topSkiperoo: .res 1

.segment "KERNEL"

.import gfx_mc_r
.import gfx_pf0
.import gfx_pf1
.import gfx_spike
.import gfx_item
.import gfx_laser
.import gfx_mc_r_ball
.import gfx_bottom_spike
.import gfx_laser_emitter

.import sound_update

.importzp _mc_framePtr
.importzp _mc_ballPtr
.importzp _mc_kernelY
.importzp laser_pos

.importzp _items_current_color

.importzp bg_blockOffset
.importzp bg_subOffset
.importzp bg_spikeNusizPtr
.importzp bg_spikePosPtr
.importzp bg_linesTop
.importzp bg_linesBottom
.importzp bg_extraTopLine
;.importzp bg_linesTopDiv2

.importzp bg_pfOffset ;bg gfx pointer offset

;a running counter of which block
; we're drawing, as a lookup into 
;block data. based on blockOffset
; but increments throughout the kernel
.exportzp kernel_blockIndex



; each block consists of
; LINE:
;  position spikes

;  bg, char
;  spikes x7
;  
; bg, char
;    next block?
;   

.macro positionSpikes
  ldy kernel_blockIndex      ;3
  ;sec
  ;lda #150
  ;sbc (bg_spikePosPtr),y  ;5 I hope
  lda (bg_spikePosPtr),y  ;5 I hope
  

  bpl :+
    sta WSYNC
    and #1
    beq @toItemSection
      jmp laser_section
@toItemSection:
    jmp item_section
    ;here we branch out to something else
    ;entirely for other things
    ;(powerups, lasers?)
:
  ldx #1                  ;2
  PosObject
.endmacro



.macro pfChunk
  ldy bg_pfOffset
  lda gfx_pf1,y 
  sta PF1      
  ;lda #%10000000
  ;sta PF0
.endmacro
  
.import gfx_p0_color
.macro mainCharChunk
  .local skip
  dec kernel_lineCtr
  lda kernel_lineCtr  
  sec                
  sbc _mc_kernelY   
  adc #14          
  bcc skip
  tay            
  lda gfx_mc_r_ball,y
  sta ENABL
  lda (_mc_framePtr),y
  .byte $2c          
skip:
  lda #0
  sta GRP0            
.endmacro


.macro spikeLine
  ldy kernel_blockIndex      ;3
  lda (bg_spikeNusizPtr),y;5/6?
  sta WSYNC
  sta NUSIZ1              ;3
  bmi @drawSpike
    lda #0
    beq @storeSpikeGrp
@drawSpike:
  lda gfx_spike,x     
@storeSpikeGrp:
  sta GRP1      

.endmacro

.macro disableVBlank
  lda #0
  sta WSYNC
  sta VBLANK

	sta WSYNC 	

.endmacro


;;;;;;;;;;;;;;;;;;;;;;;;;;;
;kernel_block:

.macro kernel_block
    positionSpikes
    ldx #7

kernel_block_top:

    sta WSYNC
    sta HMOVE
    mainCharChunk
    spikeLine
    pfChunk
    dex
    
kernel_block_loop:

    sta WSYNC
    mainCharChunk
    spikeLine
  sta HMCLR
    dex
  bne kernel_block_loop

  sta WSYNC
  mainCharChunk

 .endmacro
  ;rts
;;;;;;;;;;;;;;;;;;;;;;;;;;;;


.macro topBlock 

  .local pfChunkyChunk
  .local loop
  .local done


  ;if it's an odd number of lines,
  ;waste one
  lda bg_linesTop
  and #1
  beq :+
    sta WSYNC
:
  ;by this point, assume the 
  ;kernel prep has set bg_linesTop
  ;to be an even number, half the 
  ;actual number of lines
  ;


  lda bg_linesTop
  clc
  lsr
  beq done
  tax
loop:
    sta WSYNC
    sta HMOVE
    lda kernel_topSkiperoo
    beq :+
      lda #0
      sta GRP1
      sta WSYNC
      jmp pfChunkyChunk

:
    spikeLine
pfChunkyChunk:
    pfChunk
    sta HMCLR
  dex 
  bne loop

done:
  dec kernel_blockIndex
  inc bg_pfOffset

.endmacro

.macro partialBlock linesVar

  .local endOfBlock
  .local loop
  ;lda #$5f
  ;sta COLUPF
  ldx linesVar
  beq endOfBlock
loop:
  sta WSYNC
  mainCharChunk
  dex
  beq endOfBlock
  sta WSYNC
  pfChunk
  dey
  dex
  bne loop

endOfBlock:
  inc bg_pfOffset

.endmacro

.export blocked_kernel
blocked_kernel:

  jsr sound_update

  ;prep first topblock line
  lda #0
  sta kernel_topSkiperoo

  inc kernel_blockIndex
  ldy kernel_blockIndex      ;3
  lda (bg_spikePosPtr),y  ;5 I hope
  bpl :+
    ;skip drawing anything
    ldy #1
    sty kernel_topSkiperoo
    ;make a fake pos object to somewhere reasonable
    ;to preserve timing
    lda #40 
:
  ldx #1                  ;2
  PosObject

  
  waitForTimer

  disableVBlank

  topBlock
  ;partialBlock bg_linesTop

  lda #95
  ;sec
  ;sbc bg_linesTopDiv2
  sta kernel_lineCtr
  ;12 fits a nice stable kernel,
  ;but we have "dead" pixels on top or bottom
  ;for scrolling
  lda #10
  sta kernel_blockCtr

mainKernelLoop:
  kernel_block
afterBlockReinsert:
  dec kernel_blockIndex
  inc bg_pfOffset
  dec kernel_blockCtr
  beq :+
    jmp mainKernelLoop

:

  partialBlock bg_linesBottom

  sta WSYNC
  lda #0
  sta PF0
  sta PF1
  sta ENABL
  sta GRP0
  sta GRP1
  lda #6
  sta NUSIZ0
  sta NUSIZ1
  ldx #SPIKE_COLOR
  stx COLUP0
  sta RESP0
  nop
  sta RESP1
  stx COLUP1
  sta REFP0
  sta WSYNC

  ;spike block
  ldx #7
:
  sta WSYNC
  lda #0
  sta PF0
  sta PF1

  lda gfx_bottom_spike,x
  sta GRP1
  sta GRP0
  sta WSYNC
  dex
  bne :-

  rts



.macro laserLine
  ldy #0
  sta WSYNC
  lda laser_pos
  beq :+
    lda gfx_laser,x     
    ldy #3
    bne :++
:
  lda gfx_laser_emitter,x
:
  sty NUSIZ1              
  sta GRP1      
  sta HMCLR

.endmacro

.proc laser_section
  lda #LASER_COLOR
  sta COLUP1
  sta WSYNC
    mainCharChunk

    lda laser_pos
    bne :+
      lda #8
:
    ldx #1
    PosObject

    ldx #6

laser_block_top:

    sta WSYNC
    sta HMOVE
    mainCharChunk
    laserLine
    pfChunk
    dex
    
kernel_block_loop:

    sta WSYNC
    mainCharChunk
    laserLine
    dex
  bne kernel_block_loop
    lda #SPIKE_COLOR
    sta COLUP1

  ;sta WSYNC
  mainCharChunk
  jmp afterBlockReinsert


.endproc


.macro itemLine
  sta WSYNC
  lda _items_current_color
  beq :+
  sta COLUP1
  lda #%00100000
  sta NUSIZ1              ;3
  lda gfx_item,x
:
  sta ENAM1
  sta HMCLR
.endmacro


.proc item_section

    sta WSYNC
    mainCharChunk

    lda #60
    ldx #3
    PosObject

    ldx #6

item_block_top:

    sta WSYNC
    sta HMOVE
    mainCharChunk
    itemLine
    pfChunk
    dex
    
kernel_block_loop:

    sta WSYNC
    mainCharChunk
    itemLine
    dex
  bne kernel_block_loop
    lda #SPIKE_COLOR
    sta COLUP1

  ;sta WSYNC
  mainCharChunk
  jmp afterBlockReinsert

  
.endproc
