
//make other IDEs, etc, happy
#ifndef __CC65__
#define __fastcall__
#define __asm //
#endif

#include "binary.h"

#define u8 unsigned char
#define s8 signed char

#define u16 unsigned int
#define s16 signed int

#define bool u8
#define true 1
#define false 0

#define FACING_RIGHT 0
#define FACING_LEFT  1

#define FLIP_HORIZ B01000000
