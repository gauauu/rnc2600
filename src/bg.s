.include "vcs.inc"
.include "constants.inc"
.import reset
.import level_nextLevel
.import leveltable_PosLo, leveltable_PosHi
.import leveltable_PosBLo, leveltable_PosBHi
.import leveltable_NusizLo, leveltable_NusizHi
.import leveltable_NusizBLo, leveltable_NusizBHi

.segment "ZEROPAGE"




.exportzp bg_blockOffset

.exportzp bg_subOffset
.exportzp bg_spikeNusizPtr
.exportzp bg_spikePosPtr
.exportzp bg_linesTop
.exportzp bg_linesBottom
.exportzp bg_masterScrollSpeed
.exportzp bg_extraTopLine
.exportzp laser_pos

;.exportzp bg_linesTopDiv2

.importzp _mc_y
.importzp _mc_animTimer
.importzp _mc_status
.importzp level_max
.importzp _timer
.importzp level_num
.importzp _items_owned
.importzp _level_readyNext

; scroll level in blocks? not sure
bg_blockOffset:    .res 1

;current scroll speed
;(updated every frame to adjust for
;jet pack)
bg_scrollSpeed:    .res 1

;master scroll speed
;(adjusted every time you go through all levels)
bg_masterScrollSpeed:.res 1

laser_pos:         .res 1

;4.4 offset - 16 pixels per block,
; 16 subpixels for smoother scrolling
bg_subOffset:      .res 1

bg_spikeNusizPtr:  .res 2

; scroll in pf pixels into gfx lookup
.exportzp bg_pfOffset
bg_pfOffset:       .res 1
bg_pfOffsetBase:   .res 1

bg_spikePosPtr:    .res 2

;number of partial-block lines at top
; and bottom (not authoritative, these
; are an optimization for the kernel)
bg_linesTop:       .res 1
bg_linesBottom:    .res 1

old_linesTop:      .res 1
bg_extraTopLine:   .res 1


;an optimization for player scroll drawing
;bg_linesTopDiv2:      .res 1

BG_SCROLL_SPEED = 6


.segment "CODE"

.proc bg_dead_update
  lda _mc_animTimer		;load value into A ("it's a black thing")
  sta COLUBK	;put the value of A into the background color register
  rts
.endproc


pfColorTable:
  .byt PF_COLOR,PF_COLOR1,PF_COLOR2,PF_COLOR3,PF_COLOR4

.export bg_update
.proc bg_update


  ;if we have the rocket, maybe speed up
  lda _items_owned+2
  beq @noRocket
@rocket:
    lda _timer
    and #%00001111
    bne @afterRocketBranch
      inc bg_scrollSpeed
      jmp @afterRocketBranch

@noRocket:
  lda bg_masterScrollSpeed
  sta bg_scrollSpeed

@afterRocketBranch:


  ldx level_num
  lda pfColorTable,x
  sta COLUPF
  

  ;update laser position
  lda _timer
  and #%01000000
  beq @laserHide

@laserShow:
  lda laser_pos

  ;init it back to the right place
  bne :+
    lda #32 - 8
:

  adc #8
  sta laser_pos
  sbc #LASER_POS_B
  bcc :+
    lda #LASER_POS_A
    sta laser_pos
:
  jmp @afterLaser

@laserHide:
  lda #0
  sta laser_pos


@afterLaser:



  ;don't update if mc dead

  lda _mc_status
  cmp #4
  bne :+
    jmp bg_dead_update
:
  


  lda bg_subOffset
  clc
  adc bg_scrollSpeed
  sta bg_subOffset

  bcc @noCarry

    lda _mc_y+1
    adc #15
    sta _mc_y+1

    inc bg_blockOffset
    dec bg_pfOffsetBase

    ;scroll bg
    bpl :+
      lda #3
      sta bg_pfOffsetBase
:

    ;see if level finished
    lda bg_blockOffset
    cmp level_max
    bne :+
      lda #1
      sta _level_readyNext
      ;jmp level_nextLevel
:

@noCarry:

 
  ; precalc the lines on top
  ; and bottom for the kernel
  lda bg_subOffset
  lsr
  lsr
  lsr
  lsr
  sta bg_linesTop


  lda #15
  sec
  sbc bg_linesTop
  sta bg_linesBottom


  lda bg_pfOffsetBase
  sta bg_pfOffset

  ;set bg colors
  lda #BG_COLOR		;load value into A ("it's a black thing")
  sta COLUBK	;put the value of A into the background color register
  ;lda #PF_COLOR		;load value into A ("it's a black thing")
  ;sta COLUPF

  ;set correct pointers
  ldx level_num
  lda _timer
  and #1
  beq @prepLevelPointersB

@prepLevelPointersA:

  lda leveltable_PosLo,x
  sta bg_spikePosPtr
  lda leveltable_PosHi,x
  sta bg_spikePosPtr+1

  lda leveltable_NusizLo,x
  sta bg_spikeNusizPtr
  lda leveltable_NusizHi,x
  sta bg_spikeNusizPtr+1

  rts

@prepLevelPointersB:

  lda leveltable_PosBLo,x
  sta bg_spikePosPtr
  lda leveltable_PosBHi,x
  sta bg_spikePosPtr+1

  lda leveltable_NusizBLo,x
  sta bg_spikeNusizPtr
  lda leveltable_NusizBHi,x
  sta bg_spikeNusizPtr+1

  rts
.endproc



.export bg_init
.proc bg_init
  lda #16
  sta bg_blockOffset

  lda LASER_POS_A
  sta laser_pos

  rts
.endproc
