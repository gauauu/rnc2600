#include "common.h"


#define ITEM_DOUBLE_JUMP 0
#define ITEM_PAUSE_JUMP 1
#define ITEM_ROCKET 2

#define ITEM_COLOR_DOUBLE_JUMP 0x2a
#define ITEM_COLOR_PAUSE_JUMP 0x56
#define ITEM_COLOR_ROCKET 0x44

void items_init(void);
void items_spawnForLevel(void);
void items_acquire(void);

extern u8 items_owned[];
#pragma zpsym ("items_owned");
extern u8 items_current;
#pragma zpsym ("items_current");
extern u8 items_current_color;
#pragma zpsym ("items_current_color");


