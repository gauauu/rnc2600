.segment "ZEROPAGE"

.exportzp _level_index
.exportzp _tempA
.exportzp _tempB
.exportzp _mc_x
.exportzp _mc_y
.exportzp _mc_vx
.exportzp _mc_vy
.exportzp _mc_status
.exportzp _mc_ascendTimer
.exportzp _mc_aReleased
.exportzp _mc_topClipped
.exportzp _mc_blockYMin
.exportzp _mc_blockYMax
.exportzp _mc_collideRight
.exportzp _mc_collideLeft
.exportzp _mc_animFrame
.exportzp _mc_animTimer
.exportzp _mc_timesJumped
.exportzp _mc_deaths
.exportzp _mc_boundaryInverted
.exportzp _mc_health
.exportzp _mc_maxHealth
.exportzp _mc_fadedTimer
.exportzp _mc_kernelY
.exportzp _mc_framePtr
.exportzp _mc_ballPtr
.exportzp _mc_facing
.exportzp _ci
.exportzp _cj
.exportzp _temp
.exportzp _temp2
.exportzp _timer
.exportzp _tempPtr
.exportzp _tempPtr2
.exportzp _tempS16
.exportzp _tempPtrA
.exportzp _tempU16
.exportzp _temp3
.exportzp _tempS8
.exportzp _splashTimer
.exportzp _tempBuffer


_level_index: .res 1
_tempA: .res 1
_tempB: .res 1


_mc_x: .res 2
_mc_y: .res 2

_mc_vx: .res 2
_mc_vy: .res 2

_mc_status: .res 1

_mc_ascendTimer: .res 1
_mc_aReleased: .res 1

_mc_topClipped: .res 1


_mc_blockYMin: .res 1
_mc_blockYMax: .res 1

_mc_collideRight: .res 1
_mc_collideLeft: .res 1

_mc_animFrame: .res 1
_mc_animTimer: .res 1

_mc_timesJumped: .res 1
_mc_deaths: .res 1

_mc_boundaryInverted: .res 1

_mc_health: .res 1
_mc_maxHealth: .res 1

_mc_fadedTimer: .res 1

_mc_kernelY: .res 1

_mc_framePtr: .res 2
_mc_ballPtr: .res 2
_mc_facing: .res 1


_ci: .res 1
_cj: .res 1

_temp: .res 1
_temp2: .res 1

_timer: .res 1

_tempPtr: .res 2
_tempPtr2: .res 2
_tempS16: .res 2

_tempPtrA: .res 2
_tempU16: .res 2

_temp3: .res 1
_tempS8: .res 1

_splashTimer: .res 1

_tempBuffer: .res 14
