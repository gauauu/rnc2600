#include "mc.h"
#include "vcs.h"
#include "items.h"
#include "common.h"
#include "binary.h"

#define ENDING_TIME (level_current == 4 && \
                     level_index < 20)




#define MC_SLIDE_SPEED  300
#define MC_LEFT_BOUNDARY 32
#define MC_RIGHT_BOUNDARY 110

#define MC_START_Y 10

#define MC_JUMP_VELOCITY 1850

#define MC_JUMP_IMPULSE 760
#define MC_DOUBLE_JUMP_IMPULSE 1000

#define MC_X_VELOCITY 1000
//#define MC_GRAVITY 150
#define MC_GRAVITY 150
#define MC_MAX_FALL 1000

#define MC_SWING_SPEED 300

#define MC_ASCEND_LIMIT 20

#define MC_TILE_CLING 0
#define MC_TILE_ASC   0x60
#define MC_TILE_DESC  0x6A

#define MC_ANIM_SPEED 10
#define MC_ANIM_MAX 3

#define MC_FADED_TIME 120

#define BUTTON_PRESSED (((*(s8*)INPT4)) >= 0)
#define JOYSTICK (*SWCHA)

#define JOYPRESSED(dir) ((JOYSTICK & dir)  == 0)

#pragma bss-name (push,"ZEROPAGE")

extern u8 gfx_mc_r;
#pragma zpsym ("gfx_mc_r");

extern u8 gfx_mc_r_ball;
#pragma zpsym ("gfx_mc_r_ball");

extern u8 gfx_mc_asc;
#pragma zpsym ("gfx_mc_asc");

extern u8 gfx_mc_rocket;
#pragma zpsym ("gfx_mc_rocket");


//TODO: move these
extern u8 level_index;
#pragma zpsym ("level_index");

extern u8 tempA;
#pragma zpsym ("tempA");

extern u8 tempB;
#pragma zpsym ("tempB");



///////////////
extern u16 mc_x;
#pragma zpsym ("mc_x");

extern u16 mc_y;
#pragma zpsym ("mc_y");

extern s16 mc_vx;
#pragma zpsym ("mc_vx");
extern s16 mc_vy;
#pragma zpsym ("mc_vy");

extern u8 mc_status;
#pragma zpsym ("mc_status");

extern u8 mc_ascendTimer;
#pragma zpsym ("mc_ascendTimer");
extern u8 mc_aReleased;
#pragma zpsym ("mc_aReleased");

extern u8 mc_topClipped;
#pragma zpsym ("mc_topClipped");

//u16 mc_screen;

#define mc_renderY (MSB(mc_y))
//u8 mc_renderY;

extern u8 mc_blockYMin;
#pragma zpsym ("mc_blockYMin");
extern u8 mc_blockYMax;
#pragma zpsym ("mc_blockYMax");

extern u8 mc_collideRight;
#pragma zpsym ("mc_collideRight");
extern u8 mc_collideLeft;
#pragma zpsym ("mc_collideLeft");

extern u8 mc_animFrame;
#pragma zpsym ("mc_animFrame");
extern u8 mc_animTimer;
#pragma zpsym ("mc_animTimer");

extern u8 mc_timesJumped;
#pragma zpsym ("mc_timesJumped");
extern s8 mc_deaths;
#pragma zpsym ("mc_deaths");

extern u8 mc_boundaryInverted;
#pragma zpsym ("mc_boundaryInverted");

extern u8 mc_health;
#pragma zpsym ("mc_health");
extern u8 mc_maxHealth;
#pragma zpsym ("mc_maxHealth");

extern u8 mc_fadedTimer;
#pragma zpsym ("mc_fadedTimer");

extern u8 mc_kernelY;
#pragma zpsym ("mc_kernelY");

extern u8* mc_framePtr;
#pragma zpsym ("mc_framePtr");
extern u8* mc_ballPtr;
#pragma zpsym ("mc_ballPtr");
extern u8 mc_facing;
#pragma zpsym ("mc_facing");

void mc_jump(void) {
  mc_topClipped = false;
  mc_ascendTimer = MC_ASCEND_LIMIT;
  if (mc_status == MC_STATUS_CLING_L) {
    mc_vx = MC_X_VELOCITY;
  } else if (mc_status == MC_STATUS_CLING_R) {
    mc_vx = -MC_X_VELOCITY;
  }
  //when double-jumping, reduce velocity a bit
  if (mc_timesJumped == 1) {
    mc_vx = mc_vx / 2;
  }
  mc_status = MC_STATUS_ASCENDING;
  mc_vy = -MC_JUMP_VELOCITY;
  mc_aReleased = false;
  ++mc_timesJumped;


  __asm__("lda #1");
  __asm__("sta sound_ch1");
  __asm__("lda #3");
  __asm__("sta sound_ch1_timer");
}

void mc_checkWall(void) {

  if (MSB(mc_x) < MC_LEFT_BOUNDARY) {
    mc_status = MC_STATUS_CLING_L;
    mc_x = (MC_LEFT_BOUNDARY << 8);
  }
  if (MSB(mc_x) > MC_RIGHT_BOUNDARY) {
    mc_status = MC_STATUS_CLING_R;
    mc_x = (MC_RIGHT_BOUNDARY << 8);
  }

}


#define MIN_Y 8
void mc_clampYRange(void) {
  if (MSB(mc_y) < MIN_Y) {
    mc_y = (MIN_Y << 8);
  }
  /*
    if (MSB(mc_y) > 240) {
        if (mc_vy > 0) {
          mc_y += (16 << 8);
          if (mc_screen > 0) {
            --mc_screen;
          }
        } else {
          mc_y -= (16 << 8);
          mc_screen++;
        }
    }
    */

}

/*
void mc_incrementDeathsX() {
  ++mc_deaths;
  //fake a BCD
  if ((mc_deaths & 0xF) == 0xA) {
    mc_deaths += 0x6;
  }
  if ((mc_deaths & 0xF0) == 0XA0) {
    mc_deaths += 0x60;
  }
  if ((mc_deaths & 0xF00) == 0XA00) {
    mc_deaths += 0x600;
  }
}
*/


extern void start(void);

void mc_dead(void) {

  if (mc_fadedTimer > 0) {
    return;
  }

  if (mc_status >= MC_STATUS_DEAD) {
    return;
  }


  //sfx_playSound(SFX_HIT);

  if (mc_health > 0) {
    --mc_health;
    mc_fadedTimer = MC_FADED_TIME;
  __asm__("lda #2");
  __asm__("sta sound_ch2");
  __asm__("lda #20");
  __asm__("sta sound_ch2_timer");
  __asm__("lda #32");
  __asm__("sta sound_ch2_freq");
    return;
  }

  //mc_incrementDeaths();
  --mc_deaths;
  
  mc_status = MC_STATUS_DEAD;
  mc_animTimer = 90;

  __asm__("lda #3");
  __asm__("sta sound_ch2");
  __asm__("lda #90");
  __asm__("sta sound_ch2_timer");
  __asm__("lda #32");
  __asm__("sta sound_ch2_freq");

}



void mc_handleJumpingMovement(void) {
  if (JOYPRESSED(JOY_LEFT)) {
    mc_vx -= MC_SWING_SPEED;
    if (mc_vx < -MC_X_VELOCITY) {
      mc_vx = -MC_X_VELOCITY;
    }
  }
  if (JOYPRESSED(JOY_RIGHT)) {
    mc_vx += MC_SWING_SPEED;
    if (mc_vx > MC_X_VELOCITY) {
      mc_vx = MC_X_VELOCITY;
    }
  }
}


#define MC_ROCKET_HORIZ 500
#define MC_ROCKET_H_MAX 1200

void mc_rocketUpdate(void) {

  mc_vy = 0;
  
  //if (ENDING_TIME) {
    //mc_vy -= 400;
  //}

  if (MSB(mc_y) < 70) {
    mc_vy += 150;
  }

  __asm__("lda bg_linesTop");
  __asm__("sta _ci");
  cj = 100 - ci;
  if (MSB(mc_y) > cj) {
    mc_y = (101 - ci) * 256;
  }

  
  if (JOYPRESSED(JOY_LEFT)) {
    mc_vx -= MC_ROCKET_HORIZ;
  }
  if (JOYPRESSED(JOY_RIGHT)) {
    mc_vx += MC_ROCKET_HORIZ;
  }
  if (mc_vx < -MC_ROCKET_H_MAX) {
    mc_vx = -MC_ROCKET_H_MAX;
  }
  if (mc_vx > MC_ROCKET_H_MAX) {
    mc_vx = MC_ROCKET_H_MAX;
  }

  mc_x += mc_vx;

  mc_vx = mc_vx / 2;

  mc_checkWall();

  mc_y += mc_vy;

  mc_clampYRange();

}


void mc_update(void) {

  if (mc_status == MC_STATUS_RESPAWN) {
    return;
  }

  if (mc_fadedTimer > 0) {
    --mc_fadedTimer;
  }

  if (mc_status == MC_STATUS_DEAD) {
    --mc_animTimer;
    if (mc_animTimer < 2) {
      if (mc_deaths < 0) {
        mc_status = MC_STATUS_GAMEOVER;
        return;
      }
      mc_status = MC_STATUS_RESPAWN;
    }
    return;
  }

  if (BUTTON_PRESSED) {
  } else {
    mc_aReleased = true;
  }

  if (items_owned[ITEM_ROCKET]) {
    mc_rocketUpdate();
    goto calcRendering;
  
  }
  
  if (mc_status < MC_STATUS_ASCENDING) {
    mc_timesJumped = 0;
    if (JOYPRESSED(JOY_DOWN)) {
      mc_vy = MC_SLIDE_SPEED;
      mc_y += mc_vy;
      //mc_clampYRange();
    }

    //do animations
    --mc_animTimer;
    if (mc_animTimer == 0) {
      ++mc_animFrame;
      mc_animTimer = MC_ANIM_SPEED;
      if (mc_animFrame > MC_ANIM_MAX) {
        mc_animFrame = 0;
      }
    }

    if ((JOYPRESSED(JOY_UP)) && (items_owned[ITEM_PAUSE_JUMP])) {
      mc_vy = -MC_SLIDE_SPEED;
      mc_y += mc_vy;
      //mc_clampYRange();
    }
#ifdef CHEAT_CTRL
    if (JOYPRESSED(JOY_LEFT)) {
      mc_x -= MC_SLIDE_SPEED;
    }
    if (JOYPRESSED(JOY_RIGHT)) {
      mc_x += MC_SLIDE_SPEED;
    }
#endif

    if (mc_aReleased && BUTTON_PRESSED) {
      mc_jump();
    }
  } else if (mc_status == MC_STATUS_ASCENDING){
    --mc_ascendTimer;

    if (BUTTON_PRESSED) {} else {
       mc_vy = 0;
      mc_status = MC_STATUS_DESCENDING;
    }

    if (mc_ascendTimer == 0) {
      mc_status = MC_STATUS_DESCENDING;
    }
    mc_vy += MC_GRAVITY;

    if (mc_timesJumped == 2) {
      mc_vy = -MC_DOUBLE_JUMP_IMPULSE;
    } else {
      mc_vy = -MC_JUMP_IMPULSE;
    }

    if (mc_topClipped) {
      mc_vy = 0;
    }

    mc_handleJumpingMovement();

    mc_y += mc_vy;
    mc_clampYRange();

    mc_x += mc_vx;


    mc_checkWall();


  } else if (mc_status == MC_STATUS_DESCENDING) {

    if (mc_aReleased && (BUTTON_PRESSED)) {
      if (mc_timesJumped < 2 && items_owned[ITEM_DOUBLE_JUMP]) {
        mc_jump();
      }
    }


#ifdef PAUSE_JUMP
    if (ctrl_current & PAD_B && items_owned[ITEM_PAUSE_JUMP]) {
      mc_vx = 0;
    }
#endif

    mc_vy += MC_GRAVITY;

    if (mc_vy > MC_MAX_FALL) {
        mc_vy = MC_MAX_FALL;
    }

    mc_handleJumpingMovement();

    mc_x += mc_vx;
    mc_y += mc_vy;
    mc_clampYRange();
    mc_checkWall();



  }

calcRendering:


  //check screen boundaries
  if ((mc_renderY > (180 - MC_COL_HEIGHT)) && (mc_renderY < (235 - MC_COL_HEIGHT))) {
    //make sure he's REALLY dead
    mc_health = 0;
    mc_fadedTimer = 0;
    mc_dead();
    return;
  } else if (mc_vy < 0 && ((mc_renderY < 10) || (mc_renderY > 235))) {
    mc_topClipped = true;
  }

 
    __asm__("lda #190");
    __asm__("sbc %v+1", mc_y);
    __asm__("lsr"); //divide by 2 for 2-line kernel
    __asm__("sta %v", mc_kernelY);


  mc_facing = 0;
  if (items_owned[ITEM_ROCKET]) {
    mc_framePtr = &gfx_mc_rocket;
    if (mc_vx < 0) {
      mc_facing = 0xff;
    }
    return;
  }

    
  if (mc_status < MC_STATUS_ASCENDING) {

    mc_ballPtr = &gfx_mc_r_ball;
      
    //draw cling
    mc_framePtr = &gfx_mc_r;
    if (mc_status != MC_STATUS_CLING_L) {
      mc_facing = 0xff;
    } 
  } else if (mc_status <= MC_STATUS_DESCENDING) {
    if (mc_vy < 0) {
      mc_framePtr = &gfx_mc_asc;
    } else {
      mc_framePtr = &gfx_mc_r;
    }
    if (mc_vx < 0) {
      mc_facing = 0xff;
    }
  }
    

}


void mc_init(void) {
  mc_fadedTimer = 0;
  mc_health = mc_maxHealth;
  mc_x = (MC_LEFT_BOUNDARY << 8);
  mc_y = (MC_START_Y << 8);
  mc_status = MC_STATUS_CLING_L;
  //mc_screen = 0;
  mc_animTimer = MC_ANIM_SPEED;
  mc_aReleased = false;

  if ((*SWCHB) & B01000000) {
    __asm__("lda #12");
    __asm__("sta bg_masterScrollSpeed");
    mc_maxHealth = 0;
  } else {
    __asm__("lda #6");
    __asm__("sta bg_masterScrollSpeed");
    mc_maxHealth = 2;
  }
  mc_health = mc_maxHealth;
}
#pragma bss-name (pop)
