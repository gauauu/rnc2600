#ifndef _COMMON_H_
#define _COMMON_H_


#include "types.h"
#include "binary.h"

extern u8 ci;
#pragma zpsym ("ci");

extern u8 cj;
#pragma zpsym ("cj");

extern u8 temp;
#pragma zpsym ("temp");

extern u8 temp2;
#pragma zpsym ("temp2");

extern u8 temp3;
#pragma zpsym ("temp3");

extern s8 tempS8;
#pragma zpsym ("tempS8");

extern u8 * tempPtr;
#pragma zpsym ("tempPtr");

extern u8 * tempPtr2;
#pragma zpsym ("tempPtr2");

extern u8 VBLANK_FLAG;
#pragma zpsym ("VBLANK_FLAG");

extern u8 timer;
#pragma zpsym ("timer");

extern u8 * tempPtrA;
#pragma zpsym ("tempPtrA");

extern u16 tempU16;
#pragma zpsym ("tempU16");

extern s16 tempS16;
#pragma zpsym ("tempS16");


#define TICKCOUNT ((u16)(*(u16*)0x6b))

#define MSB(x)			((u8)(((u16)x)>>8))
#define LSB(x)			((u8)(((u16)x)&0xff))


#define LOAD_PTR_FROM_ARRAYS(dest,sourceLo,sourceHi,index)  \
  __asm__("ldx %v", index); \
  __asm__("lda %v,x", sourceLo); \
  __asm__("sta %v", dest); \
  __asm__("lda %v,x", sourceHi); \
  __asm__("sta %v+1", dest); \


#endif



