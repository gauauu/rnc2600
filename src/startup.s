.include "vcs.inc"
.include "constants.inc"
.export reset
.import main
.import title
.import _mc_init
.import bg_init
.import level_initLevel
.import level_screen
.import continue_screen
.import blocked_kernel
.import win_screen

.export bank_main
.export bank_showTitleScreen
.export bank_showLevelScreen
.export bank_showContinueScreen
.export bank_showWinScreen
.export bank_kernel
.import main_afterKernel

.importzp bg_masterScrollSpeed
.importzp _mc_deaths

.segment "RESET0"

.macro setBank0
  lda $1ff8
.endmacro

.macro setBank1
  lda $1ff9
.endmacro

.export reset
bank_showTitleScreen:
  setBank0
  jmp title

bank_main:
  setBank0
  ;jsr level_initLevel
  jmp main

bank_showLevelScreen:
  setBank0
  jmp level_screen

bank_showContinueScreen:
  setBank0
  jmp continue_screen

bank_showWinScreen:
  setBank0
  jmp win_screen
 
bank_kernel:
  setBank1
  jsr blocked_kernel

bank_afterkernel:
  setBank0
  jmp main_afterKernel


reset:

  ;bank switch to bank 0
  setBank0

  ;clear ram and flags
  sei	
	cld  	
	ldx #$FF 
	txs
	lda #0	
:
	sta 0,x	
	dex		
	bne :-	


  ;black bg
  lda #BG_COLOR		;load value into A ("it's a black thing")
  sta COLUBK	;put the value of A into the background color register
  lda #PF_COLOR		;load value into A ("it's a black thing")
  sta COLUPF

  lda #%01110001 ;8px ball, reflected pf
  sta CTRLPF

  ;set start scroll speed
  lda #6
  sta bg_masterScrollSpeed

  lda #3
  sta _mc_deaths

  jsr _mc_init
  jsr bg_init
  jmp bank_showTitleScreen


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;clone of bank jumping routines
.segment "RESET1"

  ;title
  setBank1
  jmp title

  ;main
  setBank0
  ;jsr level_initLevel
  jmp main

  ;level screen
  setBank1
  jmp level_screen

  ;continue
  setBank1
  jmp continue_screen

  ;win
  setBank1
  jmp win_screen

  ;kernel
  setBank0
  jsr blocked_kernel
  setBank0
  jmp main_afterKernel

  ;start
  setBank0
