.include "vcs.inc"
.importzp _tempBuffer

.setcpu "6502X"

.segment "TITLE"

.importzp _ci
.importzp _cj
.importzp _splashTimer
.importzp level_num
.importzp _mc_deaths
.importzp level_looped


.import Digits
.import PDigitBlank


title_done = _ci
title_debounce = _cj

.export title_logic
.proc title_logic
  lda _splashTimer
  beq :+
    dec _splashTimer
    lda #0
    sta title_done
    rts
:

  ; if reset switch pressed, start
  ; if button pressed, start
  ifButtonHeld @maybeEndTitle
  lda SWCHB
  and #1
  beq @maybeEndTitle


  ;debounce inputs if they're not pushed
  lda #0
  sta title_debounce
  sta title_done

  rts

@maybeEndTitle:
  lda title_debounce
  bne @debouncingNeverMind

    lda #1
    sta title_done

@debouncingNeverMind:
  rts

.endproc

.export title_overscan
.proc title_overscan

  lda #2		;#2 for the VBLANK...
	sta WSYNC  	;Finish this final scanline.
	sta VBLANK 	; Make TIA output invisible for the overscan, 
    
  ;set overscan timer
  lda #35
  sta TIM64T


  ;wait for overscan to finish
:
    lda INTIM
    bne :-
    sta WSYNC
    sta WSYNC

  rts


.endproc

;thanks to http://www.qotile.net/minidig/code/bigmove.asm


.macro title_kernel_init

  
  lda #0
  sta WSYNC
  sta VBLANK

	sta WSYNC 	
	lda #$00
	sta GRP0
	sta GRP1	

  lda #$b8
  sta COLUP0
  sta COLUP1
  ldy #191

    ;show a border thing
  lda #%00000001
  sta PF0
  lda #%10010000
  sta PF1



  lda     #%011
  sta     NUSIZ0
  sta     NUSIZ1
  sta     VDELP0
  sta     VDELP1


  
  sta     WSYNC
  SLEEP31
  nop
  nop
  nop
  sta     RESP0
  sta     RESP1

  lda     #$10
  sta     HMP0

  lda     #$20
  sta     HMP1

  sta     WSYNC
  sta     HMOVE

.endmacro


.macro burnLines lines
  ldy #lines
:
  sty WSYNC
  dey
  bne :-

.endmacro


.macro drawTitleBmp lines,bmp_name

  .repeat 6, I
    .import .ident(.concat("bmp_", .string(bmp_name), "_", .string(I)))

    lda #<.ident(.concat("bmp_", .string(bmp_name),"_",.string(I)))
    sta _tempBuffer+(I*2)
    lda #>.ident(.concat("bmp_", .string(bmp_name),"_",.string(I)))
    sta _tempBuffer+(I*2+1)

  .endrepeat
  lda #lines
  jsr Draw6Char

;
  ;lda #<.concat("bmp_", bmp_name,"_1")
  ;sta _tempBuffer+2
  ;lda #>.concat("bmp_", bmp_name,"_1")
  ;sta _tempBuffer+3

.endmacro

.export title_kernel
.proc title_kernel

    title_kernel_init
    burnLines 30

    drawTitleBmp 41,title

    burnLines 40

    lda SWCHB
    and #%01000000
    beq @easyDisplay

@hardDisplay:
    
    lda #$3a
    sta COLUP0
    sta COLUP1
    drawTitleBmp 10,hard
    jmp @burnLines

@easyDisplay:

    lda #$0f
    sta COLUP0
    sta COLUP1
    drawTitleBmp 10,easy

@burnLines:
    burnLines 61

  rts
.endproc



.macro prepDigit slot
  and #$0f
  asl
  asl
  asl
  clc
  adc #<Digits
  sta _tempBuffer + (slot * 2)
  lda #>Digits
  adc #0
  sta _tempBuffer + (slot * 2) + 1
.endmacro

.proc level_prepBlankDigits
;prep the blanks
  lda #<PDigitBlank
  sta _tempBuffer
  sta _tempBuffer+(5*2) 
  lda #>PDigitBlank
  sta _tempBuffer+1
  sta _tempBuffer+(5*2)+1
  rts

.endproc


.macro showLevelNumber


  lda #<PDigitBlank
  sta _tempBuffer+2
  lda #>PDigitBlank
  sta _tempBuffer+3

  ;get level, multiply by 8
  ;to find digit address
  lda level_num
  asl
  asl
  asl
  clc
  adc #<Digits
  sta _tempBuffer
  lda #>Digits
  adc #0
  sta _tempBuffer+1




  lda #71
  ldx #0
  stx COLUP1
  PosObject

  lda level_looped
  beq @noLoop


@prepLoopedDigits:
  lda #$b8
  sta COLUP1
  lda level_looped
  prepDigit 1
  lda #58
  ldx #1
  PosObject
  lda #82
  ldx #0
  PosObject
  lda #74
  ldx #3
  PosObject
  jmp @doneWithLooped

@noLoop:
  ;dummy to make lines match up
  lda #0
  ldx #3
  PosObject
  PosObject
  sta WSYNC
  sta WSYNC

@doneWithLooped:



  ldx #0
  stx VDELP0
  stx VDELP1

  lda #5
  sta NUSIZ0
  lda #%00110101
  sta NUSIZ1


  sta WSYNC
  nop
  nop
  sta HMOVE
  nop
  nop
  nop
  nop
  nop
  nop
  nop
  sta HMCLR

  lda #$b8
  sta COLUP0


  sta WSYNC

  ldy #8
:
  lda (_tempBuffer),y
  sta GRP0
  lda (_tempBuffer+2),y
  sta GRP1
  sta WSYNC
    tya
    cmp #4
    bne :+
    lda #$ff
    Skip2Bytes
:
      lda #$0
    sta ENAM1
  sta WSYNC
  dey
  bpl :--


.endmacro


title_gfx_mc_r:
        .byte %00000000;--
        .byte %01100000;--
        .byte %10110010;--
        .byte %10110010;--
        .byte %01100010;--
        .byte %00011110;--
        .byte %00111100;--
        .byte %11100000;--
        .byte %00011110;--
        .byte %00100101;--
        .byte %01000001;--
        .byte %01000101;--
        .byte %00111110;--
        .byte %00011100;--
        .byte %00000000;--

lives_nusiz_lookup:
  .byte 0
  .byte 0
  .byte 1
  .byte 3


.macro showLives

  lda #60
  ldx #0
  PosObject

  sta WSYNC
  sta HMOVE

  lda #0
  sta COLUP0

  lda _mc_deaths
  beq @noLives
    tax
    lda lives_nusiz_lookup,x
    sta NUSIZ0
    lda #$b8
    sta COLUP0

@noLives:
    
    ldy #14
  :
    lda title_gfx_mc_r,y
    sta GRP0
    sta WSYNC
    dey
    bpl :-

  lda #0
  sta GRP0



.endmacro

.export levelscreen_kernel
.proc levelscreen_kernel
  title_kernel_init

  lda #0
  sta REFP0
  
  ;show a border thing
  lda #%00000001
  sta PF0
  lda #%10010000
  sta PF1

  burnLines 40
  drawTitleBmp 11,level
  lda #0
  sta GRP0
  sta GRP1
  showLevelNumber
  burnLines 21
  showLives
  burnLines 72 

  rts
.endproc


.importzp _temp
.importzp _temp2
.align 256
.proc Draw6Char
;---------------------------------------
;  General reusable 6-char kernel chunk
;---------------------------------------

    sty WSYNC

    SLEEP51

    ;wasting more cycles:
    ldy     #00 ;3
    tay         ;2
    tay         ;2
    tay         ;2
    tay         ;2
:
    sty     _temp              ; 3

    ;setting color...these are pretty unnecessary, can waste 8 cycles 
    ;doing something more productive if I need to....
    tya                         ;2
    adc     0                   ;3
    lda     0                   ; 3 = 11

    lda     (_tempBuffer+$0),y          ; 5
    sta     GRP0                ; 3
    lda     (_tempBuffer+$2),y          ; 5
    sta     GRP1                ; 3
    lda     (_tempBuffer+$4),y          ; 5
    sta     GRP0                ; 3
    lda     (_tempBuffer+$6),y          ; 5
    sta     _temp2              ; 3 = 32

    lax     (_tempBuffer+10),y          ; 5
    lda     (_tempBuffer+$8),y          ; 5
    ldy     _temp2              ; 3
    sty     GRP1                ; 3         @43
    sta     GRP0                ; 3
    stx     GRP1                ; 3
    sta     GRP0                ; 3 = 25

    ldy     _temp               ; 3
    dey                         ; 2

    bpl     :-
    iny
    sty     COLUP0
    sty     COLUP1
    sty     GRP0
    sty     GRP1

    rts
.endproc
