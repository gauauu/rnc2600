.include "vcs.inc"
.include "constants.inc"

.segment "ZEROPAGE"

.exportzp sound_ch1
.exportzp sound_ch1_timer
.exportzp sound_ch2
.exportzp sound_ch2_timer
.exportzp sound_ch2_freq

sound_ch1:       .res 1
sound_ch1_timer: .res 1

sound_ch2:       .res 1
sound_ch2_timer: .res 1
sound_ch2_freq:  .res 1

.segment "SOUND"


.export sound_update
.proc sound_update

@sound1:

  lda sound_ch1
  beq @sound2
    lda #14
    sta AUDF0
    lda #8
    sta AUDC0
    lda #10
    sta AUDV0
    
    dec sound_ch1_timer
    bne @sound2
      lda #0
      sta AUDC0
      sta AUDV0
      sta sound_ch1

@sound2:
  lda sound_ch2
  beq @rts
    cmp #SOUND_HURT
    bne @maybeDeath
      inc sound_ch2_freq
      lda sound_ch2_freq
      sta AUDF1
      lda #10
      sta AUDV1
      lda #1 
      sta AUDC1
      jmp @endCh2


@maybeDeath:
    cmp #SOUND_DEATH
    bne @maybeItem
      inc sound_ch2_freq
      lda sound_ch2_freq
      sta AUDF1
      lda #10
      sta AUDV1
      lda #7 
      sta AUDC1
      jmp @endCh2

@maybeItem:
    cmp #SOUND_ITEM
    bne @endCh2
      lda sound_ch2_timer
      and #%11
      bne :+
        lda sound_ch2_freq
        sec
        sbc #8
        sta sound_ch2_freq
        sta AUDF1

:

      lda #10
      sta AUDV1
      lda #4 
      sta AUDC1
      jmp @endCh2

@endCh2:
    dec sound_ch2_timer
    bne @rts
      lda #0
      sta AUDC1
      sta AUDV1
      sta sound_ch2
@rts:
  rts

.endproc
