.include "vcs.inc"
.include "constants.inc"

.segment "CODE"


.import kernel
.import blocked_kernel
.import bg_update
.import bank_kernel
.import reset

.import _mc_update
.import _mc_dead
.import level_init
.import level_initLevel
.import level_nextLevel
.import _items_acquire

.importzp _mc_x
.importzp _timer
.importzp kernel_blockIndex
.importzp _mc_health
.importzp _mc_maxHealth
.importzp _mc_fadedTimer
.importzp _mc_status
.importzp _splashTimer
.importzp _mc_facing


.importzp bg_blockOffset
.importzp bg_subOffset
.importzp _level_readyNext

.macro vsyncAndSetTimer

  ;vsync and set timer
  lda  #2
	sta  VSYNC	
	sta  WSYNC	
	sta  WSYNC 
	sta  WSYNC
  
  ;set timer
  lda  #43	
	sta  TIM64T	

	lda #0		;Zero out the VSYNC
	sta  VSYNC

.endmacro


.macro gameLogic
  jsr bg_update
  jsr _mc_update

.endmacro

.macro prepKernel

  lda #PLAYER_COLOR
  sta COLUP0

  lda _mc_maxHealth
  beq :+
    ;calc player color
    ldx _mc_health
    lda playerHealthTable,x
    sta COLUP0
:


  ;see if we're faded
  lda _mc_fadedTimer
  beq :+
    lda _timer
    and #1
    beq :+
      lda #0
      sta COLUP0
:

  lda #SPIKE_COLOR
  sta COLUP1
  ldy #191

  lda #%00000101
  sta NUSIZ0
  sta HMCLR

  lda #$00
	sta GRP0
	lda #$00
	sta GRP1	

  lda #7
  ldx _mc_facing
  stx REFP0
  beq :+
    lda #5
:
  clc
  adc _mc_x+1
  ldx #4
  PosObject
  nop
  nop
  ;position player0
  ldx #0
  lda _mc_x+1
  PosObject
  ;lda _mc_x+1
  ;clc
  ;adc #7
  ;ldx #4
  ;PosObject
  
  sta WSYNC
  nop
  nop
  nop
  nop
  nop
  nop
  sta WSYNC
  sta HMOVE

  lda bg_blockOffset
  sta kernel_blockIndex

  lda #0
  nop
  nop
  nop
  nop
  nop
  nop
  nop
  nop
  nop
  sta VDELP0
  sta VDELP1
  sta HMCLR

.endmacro

.macro checkCollisions
  lda CXPPMM
  bpl :+
    jsr _mc_dead
:
 
  lda CXM1P
  bpl noCollision
    jsr _items_acquire

noCollision:
  sta CXCLR

.endmacro

.macro overscan
  lda #2		;#2 for the VBLANK...
	sta WSYNC  	;Finish this final scanline.
	sta VBLANK 	; Make TIA output invisible for the overscan, 
    
  ;set overscan timer
  lda #35
  sta TIM64T

  checkCollisions

  ;wait for overscan to finish
OverscanWait:
    lda INTIM
    bne OverscanWait
    sta WSYNC
    sta WSYNC

  ;ldx #30		
;:
  ;sta WSYNC
  ;dex
  ;bne :-
.endmacro

playerHealthTable:
 .byte PLAYER_COLOR3, PLAYER_COLOR2, PLAYER_COLOR

.export main
.export main_afterKernel
main:

  lda SWCHB
  and #1
  bne :+
    jmp reset
:

  vsyncAndSetTimer

  inc _timer
  gameLogic
  prepKernel

  jmp bank_kernel

main_afterKernel:
  overscan
  lda _level_readyNext
  beq :+
    jmp level_nextLevel
:
  lda _mc_status
  cmp #5 ;MC_STATUS_RESPAWN
  bne :+
    jmp level_screen
:
  cmp #6 ;MC_STATUS_GAMEOVER
  bne :+
    jmp reset
:
  jmp main



.segment "TITLE"


.proc title_vsyncAndSetTimer

  ;vsync and set timer
  lda  #2
	sta  VSYNC	
	sta  WSYNC	
	sta  WSYNC 
	sta  WSYNC
  
  ;set timer
  lda  #43	
	sta  TIM64T	

	lda #0		;Zero out the VSYNC
	sta  VSYNC
  rts

.endproc

.import title_logic
.import title_kernel
.import title_overscan
.import bank_main
.importzp _ci
.importzp _cj
.importzp level_num
.export title
.proc title

  ;clear out any lingering sounds
  lda #0
  sta AUDC0
  sta AUDC1
  sta AUDV0
  sta AUDV1

  ;set debouncer
  lda #1
  sta _cj
  lda #30
  sta _splashTimer

@titleScreenLoop:
  ;set not done
  lda #0
  sta _ci

  jsr title_vsyncAndSetTimer
  jsr title_logic


  waitForTimer
  jsr title_kernel
  jsr title_overscan
  lda _ci
  beq @titleScreenLoop
  lda #0
  sta level_num
  jmp level_screen
  ;jmp bank_main

.endproc

pfTitleColorTable:
  .byt PF_COLOR,PF_COLOR1,PF_COLOR2,PF_COLOR3,PF_COLOR4



.import levelscreen_kernel
.importzp level_looped
.export level_screen
.proc level_screen


  ;clear out any lingering sounds
  lda #0
  sta AUDC0
  sta AUDC1
  sta AUDV0
  sta AUDV1

  sta COLUBK

  ldx level_num
  lda pfTitleColorTable,x
  sta COLUPF

  ;set debouncer
  lda #1
  sta _cj
  lda #30
  sta _splashTimer

@levelScreenLoop:
  ;set not done
  lda #0
  sta _ci

  jsr title_vsyncAndSetTimer
  jsr title_logic


  ;if we're done, load the level
  lda _ci
  beq :+
    jsr level_initLevel
:


  waitForTimer
  jsr levelscreen_kernel
  jsr title_overscan
  lda _ci
  beq @levelScreenLoop
  jmp bank_main

.endproc

.export continue_screen
.proc continue_screen

  jmp main
.endproc

.export win_screen
.proc win_screen

  jmp main
.endproc

