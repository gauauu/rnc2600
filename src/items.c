#include "items.h"
#include "mc.h"

#define NUM_ITEMS 4

#define ITEM_TILE_DOUBLE_JUMP 0xDE
#define ITEM_TILE_PAUSE_JUMP 0xD4
#define ITEM_TILE_ROCKET 0xD2
#pragma bss-name (push,"ZEROPAGE")

u8 items_owned[NUM_ITEMS];
u8 items_current;
u8 items_current_color;
#pragma bss-name (pop)


void items_init(void){
  for (cj = 0; cj < NUM_ITEMS; ++cj) {
    //auto-add double jump 
    if (cj == ITEM_DOUBLE_JUMP && level_current > 2) {
      items_owned[cj] = 1;
      continue;
    }
    //auto-add pause jump
    if (cj == ITEM_PAUSE_JUMP && level_current > 3) {
      items_owned[cj] = 1;
      continue;
    }
    items_owned[cj] = 0;
  }
  items_current = 0;
  items_current_color = 0;

}


void items_spawnForLevel(void) {

  if (level_current == 0) {
    items_current = ITEM_ROCKET;
    items_current_color = ITEM_COLOR_DOUBLE_JUMP;
  }
  if (level_current == 2) {
    items_current = ITEM_DOUBLE_JUMP;
    items_current_color = ITEM_COLOR_DOUBLE_JUMP;
  }
  if (level_current == 3) {
    items_current = ITEM_PAUSE_JUMP;
    items_current_color = ITEM_COLOR_PAUSE_JUMP;
  }
  if (level_current == 4) {
    items_current = ITEM_ROCKET;
    items_current_color = ITEM_COLOR_ROCKET;
  }
}


void items_acquire(void) {
  //sfx_playSound(SFX_POWERUP);
  items_owned[items_current] = 1;
  items_current = 0; 
  items_current_color = 0;

  
  __asm__("lda #4");
  __asm__("sta sound_ch2");
  __asm__("lda #15");
  __asm__("sta sound_ch2_timer");
  __asm__("lda #32");
  __asm__("sta sound_ch2_freq");


}
