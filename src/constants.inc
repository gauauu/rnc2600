SPIKE_COLOR   = $38
PLAYER_COLOR  = $c6
PLAYER_COLOR2 = $cA
PLAYER_COLOR3 = $0A
BG_COLOR      = $00
LASER_COLOR   = $ad

VISOR_COLOR   = $9a
WHEEL_COLOR   = $28

LASER_POS_A   = 32
LASER_POS_B   = 80

PF_COLOR      = $1b
PF_COLOR1     = $5b
PF_COLOR2     = $9b
PF_COLOR3     = $f7
PF_COLOR4     = $0d

LOOP_SCROLL_INCREMENT  = 7
LOOP_MAX_SCROLL        = 32


SOUND_JUMP    = 1
SOUND_HURT    = 2
SOUND_DEATH   = 3
SOUND_ITEM    = 4
