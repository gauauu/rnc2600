#include "binary.h"

#ifndef __VCS_H_
#define __VCS_H_

#define VSYNC   (volatile u8*)0x00
#define VBLANK  (volatile u8*)0x01
#define WSYNC   (volatile u8*)0x02
#define RSYNC   (volatile u8*)0x03
#define NUSIZ0  (volatile u8*)0x04
#define NUSIZ1  (volatile u8*)0x05
#define COLUP0  (volatile u8*)0x06
#define COLUP1  (volatile u8*)0x07
#define COLUPF  (volatile u8*)0x08
#define COLUBK  (volatile u8*)0x09
#define CTRLPF  (volatile u8*)0x0A
#define REFP0   (volatile u8*)0x0B
#define REFP1   (volatile u8*)0x0C
#define PF0     (volatile u8*)0x0D
#define PF1     (volatile u8*)0x0E
#define PF2     (volatile u8*)0x0F
#define RESP0   (volatile u8*)0x10
#define POSH2   (volatile u8*)0x11
#define RESP1   (volatile u8*)0x11
#define RESM0   (volatile u8*)0x12
#define RESM1   (volatile u8*)0x13
#define RESBL   (volatile u8*)0x14
#define AUDC0   (volatile u8*)0x15
#define AUDC1   (volatile u8*)0x16
#define AUDF0   (volatile u8*)0x17
#define AUDF1   (volatile u8*)0x18
#define AUDV0   (volatile u8*)0x19
#define AUDV1   (volatile u8*)0x1A
#define GRP0    (volatile u8*)0x1B
#define GRP1    (volatile u8*)0x1C
#define ENAM0   (volatile u8*)0x1D
#define ENAM1   (volatile u8*)0x1E
#define ENABL   (volatile u8*)0x1F
#define HMP0    (volatile u8*)0x20
#define HMP1    (volatile u8*)0x21
#define HMM0    (volatile u8*)0x22
#define HMM1    (volatile u8*)0x23
#define HMBL    (volatile u8*)0x24
#define VDELP0  (volatile u8*)0x25
#define VDELP1  (volatile u8*)0x26
#define VDELBL  (volatile u8*)0x27
#define RESMP0  (volatile u8*)0x28
#define RESMP1  (volatile u8*)0x29
#define HMOVE   (volatile u8*)0x2A
#define HMCLR   (volatile u8*)0x2B
#define CXCLR   (volatile u8*)0x2C
#define CXM0P   (volatile u8*)0x30
#define CXM1P   (volatile u8*)0x31
#define CXP0FB  (volatile u8*)0x32
#define CXP1FB  (volatile u8*)0x33
#define CXM0FB  (volatile u8*)0x34
#define CXM1FB  (volatile u8*)0x35
#define CXBLPF  (volatile u8*)0x36
#define CXPPMM  (volatile u8*)0x37
#define INPT0   (volatile u8*)0x38
#define INPT1   (volatile u8*)0x39
#define INPT2   (volatile u8*)0x3A
#define INPT3   (volatile u8*)0x3B
#define INPT4   (volatile u8*)0x3C
#define INPT5   (volatile u8*)0x3D
#define SWCHA   (volatile u8*)0x0280
#define SWACNT  (volatile u8*)0x281
#define SWCHB   (volatile u8*)0x282
#define SWBCNT  (volatile u8*)0x283
#define INTIM   (volatile u8*)0x284
#define TIMINT  (volatile u8*)0x285
#define TIM1T   (volatile u8*)0x294
#define TIM8T   (volatile u8*)0x295
#define TIM64T  (volatile u8*)0x296
#define T1024T  (volatile u8*)0x297
#define TIM1I   (volatile u8*)0x29c
#define TIM8I   (volatile u8*)0x29d
#define TIM64I  (volatile u8*)0x29e
#define T1024I  (volatile u8*)0x29f



#define JOY_LEFT    B01000000
#define JOY_RIGHT   B10000000
#define JOY_UP      B00010000
#define JOY_DOWN    B00100000

#define JOY_BUTTON_PRESSED B10000000

#define MIRROR_CLEAR B00000000 
#define MIRROR_SET   B00001000 

#define HMOVE_LEFT_7 B01110000
#define HMOVE_LEFT_6 B01100000
#define HMOVE_LEFT_5 B01010000
#define HMOVE_LEFT_4 B01000000
#define HMOVE_LEFT_3 B00110000
#define HMOVE_LEFT_2 B00100000
#define HMOVE_LEFT_1 B00010000
#define HMOVE_NONE   B00000000
#define HMOVE_RIGHT_1 B11110000
#define HMOVE_RIGHT_2 B11100000
#define HMOVE_RIGHT_3 B11010000
#define HMOVE_RIGHT_4 B11000000
#define HMOVE_RIGHT_5 B10110000
#define HMOVE_RIGHT_6 B10100000
#define HMOVE_RIGHT_7 B10010000

#define NUSIZ_MISSILE_1 B00000000
#define NUSIZ_MISSILE_4 B00100000
#define NUSIZ_MISSILE_8 B00110000

#define NUSIZ_COPIES_1       B00000000
#define NUSIZ_COPIES_3_MED   B00000110
#define NUSIZ_COPIES_3_CLOSE B00000011
#define NUSIZ_COPIES_2_CLOSE B00000001

#define COLLISION_M0_P1 B10000000
#define COLLISION_P0_P1 B10000000
#define COLLISION_P1_PF B10000000
#define COLLISION_P0_PF B10000000


#endif
