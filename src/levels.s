.include "vcs.inc"
.include "constants.inc";


.import level0Pos
.import level0Nusiz
.import bank_showLevelScreen
.import bg_init
.import _mc_init
.import _items_spawnForLevel
.import _items_init

.importzp bg_spikeNusizPtr
.importzp bg_spikePosPtr
.importzp bg_masterScrollSpeed
.importzp _tempPtr


.exportzp level_num
.exportzp level_max
.exportzp _level_current
.exportzp _level_readyNext
.exportzp level_looped

.segment "ZEROPAGE"
_level_current:
level_num:  .res 1
level_max:  .res 1
_level_readyNext: .res 1

level_looped: .res 1


.segment "CODE"


.export leveltable_PosLo, leveltable_PosHi
.export leveltable_PosBLo, leveltable_PosBHi
.export leveltable_NusizLo, leveltable_NusizHi
.export leveltable_NusizBLo, leveltable_NusizBHi

.import  level0Pos,level1Pos,level2Pos,level3Pos,level4Pos
leveltable_PosLo:
  .lobytes level0Pos,level1Pos,level2Pos,level3Pos,level4Pos
leveltable_PosHi:
  .hibytes level0Pos,level1Pos,level2Pos,level3Pos,level4Pos

.import  level0Nusiz,level1Nusiz,level2Nusiz,level3Nusiz,level4Nusiz
leveltable_NusizLo:
  .lobytes level0Nusiz,level1Nusiz,level2Nusiz,level3Nusiz,level4Nusiz
leveltable_NusizHi:
  .hibytes level0Nusiz,level1Nusiz,level2Nusiz,level3Nusiz,level4Nusiz

.import  level0PosB,level1PosB,level2PosB,level3PosB,level4PosB
leveltable_PosBLo:
  .lobytes level0PosB,level1PosB,level2PosB,level3PosB,level4PosB
leveltable_PosBHi:
  .hibytes level0PosB,level1PosB,level2PosB,level3PosB,level4PosB

.import  level0NusizB,level1NusizB,level2NusizB,level3NusizB,level4NusizB
leveltable_NusizBLo:
  .lobytes level0NusizB,level1NusizB,level2NusizB,level3NusizB,level4NusizB
leveltable_NusizBHi:
  .hibytes level0NusizB,level1NusizB,level2NusizB,level3NusizB,level4NusizB

.import  level0_max,level1_max,level2_max,level3_max,level4_max
leveltable_maxLo:
  .lobytes level0_max,level1_max,level2_max,level3_max,level4_max
leveltable_maxHi:
  .hibytes level0_max,level1_max,level2_max,level3_max,level4_max



;initializes the level system,
; sets to the first level?
.import _level0_max
.export level_init
.proc level_init

  lda #0
  sta level_num

  lda _level0_max
  adc #8
  sta level_max

  loadPointer level0Pos,bg_spikePosPtr
  loadPointer level0Nusiz,bg_spikeNusizPtr

  jsr bg_init

  rts
.endproc

;initializes next level based on level_num
.export level_initLevel
.proc level_initLevel
  
  ldx level_num

  lda leveltable_PosLo,x
  sta bg_spikePosPtr
  lda leveltable_PosHi,x
  sta bg_spikePosPtr+1

  lda leveltable_NusizLo,x
  sta bg_spikeNusizPtr
  lda leveltable_NusizHi,x
  sta bg_spikeNusizPtr+1

  lda leveltable_maxLo,x
  sta _tempPtr
  lda leveltable_maxHi,x
  sta _tempPtr+1
  ldy #0
  lda (_tempPtr),y

  adc #13
  sta level_max

  jsr bg_init
  jsr _mc_init


  jsr _items_init
  jsr _items_spawnForLevel


  rts
.endproc

.proc level_victory

.endproc

.export level_nextLevel
.proc level_nextLevel
  inc level_num
  lda level_num
  cmp #5
  bne :+
    sed
    lda level_looped
    clc
    adc #1
    sta level_looped
    cld

    lda #0
    sta level_num
    lda bg_masterScrollSpeed
    adc #LOOP_SCROLL_INCREMENT
    sta bg_masterScrollSpeed
    sbc #LOOP_MAX_SCROLL
    bcc :+
      lda #LOOP_MAX_SCROLL
      sta bg_masterScrollSpeed
:
  lda #0
  sta _level_readyNext
  jmp bank_showLevelScreen

.endproc
