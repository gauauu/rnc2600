.segment "TITLE"

.export Digits
Digits:
PDigit0:
        .byte %00000000
        .byte %00111000
        .byte %01000100
        .byte %01000100
        .byte %01000100
        .byte %01000100
        .byte %00111000
        .byte %00000000

PDigit1:

        .byte %00000000
        .byte %01111110
        .byte %00011000
        .byte %00011000
        .byte %00011000
        .byte %00111000
        .byte %00011000
        .byte %00000000
PDigit2:
        .byte %00000000
        .byte %01111100
        .byte %01100000
        .byte %00111000
        .byte %00000100
        .byte %01100100
        .byte %00111000
        .byte %00000000

PDigit3:
        .byte %00000000
        .byte %00111000
        .byte %01100100
        .byte %00001100
        .byte %00001000
        .byte %01100100
        .byte %00111000
        .byte %00000000
PDigit4:
        .byte %00000000
        .byte %00000100
        .byte %00000100
        .byte %00111110
        .byte %00010100
        .byte %00001100
        .byte %00000100
        .byte %00000000
PDigit5:
        .byte %00000000
        .byte %01111100
        .byte %00000010
        .byte %01111100
        .byte %01000000
        .byte %01000000
        .byte %01111100
        .byte %00000000

PDigit6:
        .byte %00000000
        .byte %00111100
        .byte %01000010
        .byte %01111100
        .byte %01000000
        .byte %01000000
        .byte %00111100
        .byte %00000000
        
PDigit7:
        .byte %00000000
        .byte %00010000
        .byte %00010000
        .byte %00010000
        .byte %00001000
        .byte %00000100
        .byte %01111100
        .byte %00000000
PDigit8:
        .byte %00000000
        .byte %00111100
        .byte %01000010
        .byte %01000010
        .byte %00111100
        .byte %01000010
        .byte %00111100
        .byte %00000000
PDigit9:
        .byte %00000000
        .byte %00111000
        .byte %00000100
        .byte %00000100
        .byte %00111100
        .byte %01000100
        .byte %00111000
        .byte %00000000

      .byte %00000000
.export PDigitBlank
.export PDigitDash
PDigitBlank:
      .byte %00000000
      .byte %00000000
      .byte %00000000
      .byte %00000000
      .byte %00000000
      .byte %00000000
      .byte %00000000
PDigitDash:
      .byte %00000000
      .byte %00000000
      .byte %00000000
      .byte %01111110
      .byte %00000000
