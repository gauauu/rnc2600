.export negax
.proc negax
        clc
        eor     #$FF
        adc     #1
        pha
        txa
        eor     #$FF
        adc     #0
        tax
        pla
        rts
.endproc

.import _temp
.export shrax3
.proc shrax3
        stx     _temp
        lsr     _temp
        ror     a
        lsr     _temp
        ror     a
        lsr     _temp
        ror     a
        ldx     _temp
        rts
.endproc



