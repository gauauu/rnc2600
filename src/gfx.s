.segment "KERNEL"

.include "constants.inc"

.export gfx_spike
        .byte 0
gfx_spike:
        .byte 0
        .byte 0
        .byte %01001010;--
        .byte %00111100;--
        .byte %11111111;--
        .byte %00111100;--
        .byte %01011010;--
        .byte 0
        .byte 0
        .byte 0

.export gfx_laser
        .byte 0
gfx_laser:
        .byte 0
        .byte 0
        .byte %00001000;--
        .byte %00010100;--
        .byte %00100010;--
        .byte %01000001;--
        .byte %10000000;--
        .byte 0
        .byte 0
        .byte 0


.export gfx_item
        .byte 0
gfx_item:
        .byte 0
        .byte 0
        .byte 0
        .byte %11111111;--
        .byte %11111111;--
        .byte %11111111;--
        .byte %00000000;--
        .byte %00000000;--
        .byte %00000000;--
        .byte %00000000;--
        .byte 0


.export gfx_mc_r
.export _gfx_mc_r
gfx_mc_r:
_gfx_mc_r:
        .byte %00000000;--
        .byte %01100000;--
        .byte %10110010;--
        .byte %10110010;--
        .byte %01100010;--
        .byte %00011110;--
        .byte %00111100;--
        .byte %11100000;--
        .byte %00011110;--
        .byte %00100101;--
        .byte %01000001;--
        .byte %01000101;--
        .byte %00111110;--
        .byte %00011100;--
        .byte %00000000;--
;---End Graphics Data---


.export gfx_mc_r_ball
.export _gfx_mc_r_ball
gfx_mc_r_ball:
_gfx_mc_r_ball:
        .byte %00000000;--
        .byte %00000000;--
        .byte %00000000;--
        .byte %00000000;--
        .byte %00000000;--
        .byte %00000000;--
        .byte %00000000;--
        .byte %00000000;--
        .byte %00000000;--
        .byte %11111111;--
        .byte %11111111;--
        .byte %11111111;--
        .byte %00000000;--
        .byte %00000000;--
        .byte %00000000;--
;---End Graphics Data---



.export gfx_mc_asc
.export _gfx_mc_asc
gfx_mc_asc:
_gfx_mc_asc:
        .byte %00000000;--
        .byte %00011000;--
        .byte %00101100;--
        .byte %00101100;--
        .byte %00011001;--
        .byte %10000001;--
        .byte %01111110;--
        .byte %00000000;--
        .byte %00111110;--
        .byte %01000101;--
        .byte %01000001;--
        .byte %01000101;--
        .byte %00111110;--
        .byte %00011100;--

.export gfx_mc_rocket
.export _gfx_mc_rocket
gfx_mc_rocket:
_gfx_mc_rocket:
        .byte %00010000;--
        .byte %00011000;--
        .byte %00111000;--
        .byte %00111000;--
        .byte %10011001;--
        .byte %10111101;--
        .byte %10111101;--
        .byte %01111110;--
        .byte %00111100;--
        .byte %01111110;--
        .byte %11000001;--
        .byte %11001001;--
        .byte %01111110;--
        .byte %00000000;--


.export gfx_p0_color
gfx_p0_color:
  .byte VISOR_COLOR
  .byte VISOR_COLOR
  .byte VISOR_COLOR
  .byte VISOR_COLOR
  .byte PLAYER_COLOR
  .byte PLAYER_COLOR
  .byte PLAYER_COLOR
  .byte PLAYER_COLOR
  .byte PLAYER_COLOR
  .byte PLAYER_COLOR
  .byte PLAYER_COLOR
  .byte VISOR_COLOR
  .byte VISOR_COLOR
  .byte PLAYER_COLOR

.export gfx_pf0, gfx_pf1
gfx_pf0:
   .byte %00110000
   .byte %01010000
   .byte %10010000
   .byte %00010000
   .byte %00010000
   .byte %00010000
   .byte %00010000
   .byte %00010000
   .byte %00010000
   .byte %10010000
   .byte %01010000
   .byte %00110000
   .byte %00110000
   .byte %01010000
   .byte %10010000
   .byte %00010000
   .byte %00010000
   .byte %00010000
   .byte %00010000
   .byte %00010000
   .byte %10010000
   .byte %01010000
   .byte %00110000
   .byte %00110000
   .byte %01010000
   .byte %10010000
   .byte %00010000
   .byte %00010000
   .byte %00010000
   .byte %00010000
   .byte %00010000
   .byte %10010000
   .byte %01010000
   .byte %00110000
gfx_pf1:
   .byte %01010000
   .byte %00110000
   .byte %01010000
   .byte %10010000
   .byte %01010000
   .byte %00110000
   .byte %01010000
   .byte %10010000
   .byte %01010000
   .byte %00110000
   .byte %01010000
   .byte %10010000
   .byte %01010000
   .byte %00110000
   .byte %01010000
   .byte %10010000


.export gfx_bottom_spike
gfx_bottom_spike:

  .byte %01111111
  .byte %00111110
  .byte %00111110
  .byte %00011100
  .byte %00011100
  .byte %00011100
  .byte %00001000
  .byte %00001000

.export gfx_laser_emitter
gfx_laser_emitter:

  .byte %11000000
  .byte %00110000
  .byte %00001100
  .byte %11110011
  .byte %11110011
  .byte %00001100
  .byte %00110000
  .byte %11000000


